from django import forms

class input_login (forms.Form):
    input_username = forms.CharField("Username", min_length=1)
    input_password = forms.CharField("Password", min_length=8)