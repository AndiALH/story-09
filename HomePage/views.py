from django.shortcuts import render, redirect
from datetime import datetime
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

# Create your views here.
def homepage(request):
    return render(request,'index.html')

def loginpage(request):
    if(request.user is not None):
        if request.user.is_authenticated:
            return redirect('HomePage:homepage')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            request.session['login_timestamp'] = str(datetime.now())
            return redirect('HomePage:homepage')
        else:
            return render(request, 'login.html', {
                'error' : True
            })        

    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    auth_logout(request)
    return redirect('HomePage:homepage')
