from django.urls import path
from . import views

app_name = 'HomePage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('login/', views.loginpage, name='loginpage'),
    path('logout/', views.logout, name='logout')
]