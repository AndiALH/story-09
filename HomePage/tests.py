from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.http import HttpRequest

# Create your tests here.
class HomePageTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super(HomePageTest, cls).setUpClass()
        cls.user = User.objects.create_user(
            'username101', 'thisisemail@gmail.com', 'password123'
        )
        cls.user.first_name = 'firstname'
        cls.user.last_name = 'lastname'
        cls.user.save()
        
    def test_homepage_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.homepage)

    def test_homepage_not_logged_in(self):
        response = self.client.get('/')
        html = response.content.decode()
        self.assertIn('You are not logged in yet', html)
        self.assertNotIn('Logged In', html)

    def test_homepage_logged_in(self):
        self.client.login(username='username101', password='password123')
        response = self.client.get('/')
        html = response.content.decode()
        self.assertIn('Logged In', html)
        self.assertNotIn('You are not logged in yet', html)

    def test_loginpage_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_already_authenticated(self):
        request = HttpRequest()
        request.user = User()
        views.loginpage(request)

    def test_login_post_succeed(self):
        response = self.client.post('/login/', {
                'username' : 'username101',
                'password' : 'password123',
            })
        self.assertEqual(response.status_code, 302)
        response_home = self.client.get('/')
        html = response_home.content.decode()
        self.assertIn(self.user.first_name, html)

    def test_login_post_fail(self):
        response = self.client.post('/login/', {
                'username' : 'username404',
                'password' : 'password12345',
            })
        self.assertEqual(response.status_code, 200)
        html = response.content.decode()
        self.assertIn('Login Page', html)

    def test_logout(self):
        response = self.client.post('/login/', {
                'username' : 'username101',
                'password' : 'password123',
            })
        response = self.client.get('/')
        html = response.content.decode()
        self.assertIn('Logged In', html)
        response2 = self.client.get('/logout/')
        self.assertEqual(response2.status_code, 302)
        response3 = self.client.get('/')
        html2 = response3.content.decode()
        self.assertIn('You are not logged in yet', html2)
